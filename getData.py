# GetAlchemy.py
# Takes a URL, sends the request to Alchemy, and receives the entities,
# saving them to a file of the user's choice.

import requests
import json
import glob
from bs4 import BeautifulSoup
from pprint import pprint

API_URL = "http://en.wikipedia.org/w/api.php"

def control():
    nominees = getNominees()
    nomdict = dict([[n['user'],n] for n in nominees])
    userInfo = getUserInfo(nomdict.keys())
    for u in userInfo:
        data = nomdict[u['name']]
        data['info'] = u
        data['contribs'] = getUserContribs(u['name'])
        fn = u['name'] + userInfo.index(u) + ".txt"
        write_file(fn,data)

def getNominees():
    nominees = []
    for text in (get_file(fn) for fn in glob.iglob('?N*.html')):
        soup = BeautifulSoup(text).find(class_='wikitable').find_all('tr')
        rows = [r.contents for r in soup if len(r.contents) == 9][1:]

        for r in rows:
            closed_by = r[5].contents
            if len(closed_by) > 0:
                closed_by = closed_by[0].contents[0]
            else:
                closed_by = ""

            tally = r[7].contents
            if len(tally) > 2:
                support = tally[2].contents[0]
                tally = tally[3][1:-1].split("/")
                oppose = tally[0]
                neutral = tally[1]
            elif len(r[7].contents) > 0:
                tally = tally[1][1:-1].split("/")
                support = tally[0]
                oppose = tally[1]
                neutral = tally[2]
            else:
                support = ""
                oppose = ""
                neutral = ""

            user = r[1].contents[0].contents[0].strip()

            if user[-2] == "(":
                user = user[:-2].strip()

            nominees.append({'user': user,
                'date': r[3].contents[0],
                'closed_by': closed_by,
                'support': support,
                'oppose': oppose,
                'neutral': neutral})

    return nominees


def get_file(fn):
    f = open(fn,'r')
    text = f.read()
    f.close()
    return text

def write_file(fn,data):
    f = open(fn,'w')
    json.dump(data,f)
    f.close()
    return text

def getUserInfo(query):
    """query must be a list!"""
    payload = {
        'action': "query",
        'list': "users",
        'format': "json",
        'maxlag': 5,
        'ususers': "|".join(query),
        'usprop': "|".join(["blockinfo","groups","implicitgroups","rights","editcount","registration","emailable","gender"])
    }

    r = requests.get(API_URL, params=payload)

    while r.ok == False:
        print "ERROR: " + r.status_code + ", " + r.header
        time.sleep(r.header['Retry-After']) 
    
    return r.json['text']['query']

def getUserContribs(query):
    newcontribs=makeQuery(query)
    contribs=newcontribs['query']['usercontribs']
    while 'query-continue' in newcontribs:
        newcontribs=makeUCQuery(query,newcontribs['query-continue']['usercontribs']['ucstart'])
        contribs.extend(newcontribs['query']['usercontribs'])

    return contribs

def makeUCQuery(query, ucstart=None):       
    payload = {
        'action': "query",
        'list': "usercontribs",
        'format': "json",
        'uclimit': 500,
        'ucuser': query,
        'ucprop': "|".join(["ids","title","timestamp","comment","size","sizediff","flags","tags"])
    }

    r = requests.get(API_URL, params=payload)

    while r.ok == False:
        print "ERROR: " + r.status_code + ", " + r.header
        time.sleep(r.header['Retry-After']) 
        
    return r.json['text']


if __name__ == '__main__':
    control()